class Jwt {

	constructor(_appObj) {
		this._configs = _appObj.Configs;
		this._props = {};
	}

	async initialize() {
		// this._props = this._configs;
		await this.configureJwt();
	}
	
	async configureJwt(){
		const secretName = this._configs.JwtSecretName;
		await TAXMANN.awsSecretsManagerObj.getSecretValue(secretName).then((data) => {
	
			let secret = JSON.parse(data);
			
			console.log("Jwt config", data);

			this.Secret = secret.secret;

		}).catch((err) => {
			console.log("Unable to fetch Jwt configuration.", err);
		});
	}
}


module.exports = Jwt;