const AWS = require('aws-sdk');

/**
 * @author Chetan Upreti
 */
class AWSS3 {

	constructor(_appObj) {
		this._props = {};
		this._awsS3 = {};
	}

	initialize() {
		this.initializeAWSS3();
	}

	//Initializes the AWSS3
	initializeAWSS3() {
		try {
			// Create a S3
			this._awsS3 = new AWS.S3();
			TAXMANN.awsS3Obj = this._awsS3;
		} catch (error) {
			console.error('Unable to create AWS S3 Instance:', error);
		}
	}

};

module.exports = AWSS3;