// const aws = require('aws-sdk');

// const AwsSecretsManager = new aws.SecretsManager({
//     apiVersion: '2017-10-17', 
//     region:'ap-south-1'
// });


// let getSecretValue = (secretName) => {
//     return new Promise((resolve, reject) => {
//         AwsSecretsManager.getSecretValue({
//             SecretId: secretName
//         }, (err, data) => {
//             if(err) reject(err);
//             else resolve(data.SecretString);
//         });
//     });
// }

// exports.getSecretValue = getSecretValue;

const AWS = require('aws-sdk');
const region = "ap-south-1";
AWS.config.update({ region: region });

class AWSSecretsManager {

    constructor() {
        this._props = {};
        this._awsSecretsManagerClient = {};
    }

    initialize() {
        this.initializeAWSSecretsManager();
    }

    //Initializes the AWSSecretsManager
    initializeAWSSecretsManager() {
        try {
            // Create a Secrets Manager client
            this._awsSecretsManagerClient = new AWS.SecretsManager({
                region: region
            });
            // TAXMANN.awsSecretsManagerObj = this._awsSecretsManagerClient;
        } catch (error) {
            console.error('Unable to create AWS SecretsManager client:', error);
        }
    }

    getSecretValue(secretName) {
        return new Promise((resolve, reject) => {
            this._awsSecretsManagerClient.getSecretValue({ SecretId: secretName }, function (err, data) {
                if (err) {
                    if (err.code === 'DecryptionFailureException')
                        // Secrets Manager can't decrypt the protected secret text using the provided KMS key.
                        // Deal with the exception here, and/or rethrow at your discretion.
                        reject(err);
                    else if (err.code === 'InternalServiceErrorException')
                        // An error occurred on the server side.
                        // Deal with the exception here, and/or rethrow at your discretion.
                        reject(err);
                    else if (err.code === 'InvalidParameterException')
                        // You provided an invalid value for a parameter.
                        // Deal with the exception here, and/or rethrow at your discretion.
                        reject(err);
                    else if (err.code === 'InvalidRequestException')
                        // You provided a parameter value that is not valid for the current state of the resource.
                        // Deal with the exception here, and/or rethrow at your discretion.
                        reject(err);
                    else if (err.code === 'ResourceNotFoundException')
                        // We can't find the resource that you asked for.
                        // Deal with the exception here, and/or rethrow at your discretion.
                        reject(err);
                }
                else {
                    // Depending on whether the secret is a string or binary, one of these fields will be populated.
                    if ('SecretString' in data) {
                        const secret = data.SecretString;
                        resolve(secret);
                    }
                }
            });
        })
    }
};

module.exports = AWSSecretsManager;