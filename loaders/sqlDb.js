const sql = require('mssql');
// const AwsSecretsManager = require('./awsSecretsManager');

class SqlDB {

	constructor(_appObj) {
		this._configs = _appObj.Configs;
		this._props = {};
	}

	async initialize() {
		// this._props = this._configs;
		await this.establishSqlDBConnection();
	}
	
	async establishSqlDBConnection(){
		const secretName = this._configs.SqlDbSecretName;
		await TAXMANN.awsSecretsManagerObj.getSecretValue(secretName).then((data) => {
	
			this._props.secret = JSON.parse(data);
			
			console.log("Sql config", data);

			try{
				sql.connect("mssql://"+ this._props.secret.username +":"+ this._props.secret.password 
					+"@"+ this._props.secret.host +"/"+ this._props.secret.database, (err) => {
					if(err) console.log(err);
					else console.log("Connected to Sql.");
				});

				this.QueryObj = sql;

			} catch(err) {
				console.log("Unable to fetch SQL DB configuration.", err);
			}

		}).catch((err) => {
			console.log("Unable to fetch SQL DB configuration.", err);
		});
	}
}


module.exports = SqlDB;