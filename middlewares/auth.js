const jwt = require('jsonwebtoken');

class Auth {

	constructor(_appObj) {
		this._configs = _appObj.Configs;
		this._props = {};
	}


	async initialize() {
		// this._props = this._configs;
		await this.fetchJwtSecretKey();
	}


	async fetchJwtSecretKey(){
		const secretName = this._configs.JWT_TOKEN_SECRET_NAME;
		await TAXMANN.awsSecretsManagerObj.getSecretValue(secretName).then((data) => {
	
			this._props.secret = JSON.parse(data);
			
			console.log("JWT config", data);

			this.JWT_SECRET_KEY = this._props.secret.secret;

		}).catch((err) => {
			console.log("Unable to fetch JWT configuration.", err);
		});
	}

	async checkToken(req, res, next) {
		try{
			let token = req.headers['taxmannauthorization'];
		    
		    if(token){
		        if(token.startsWith('Bearer')) token = token.slice(6, token.length);
		        
		        jwt.verify(token, TAXMANN.Middlewares.Auth.JWT_SECRET_KEY, (err, decoded) => {
		            if(err) TAXMANN.Utils.customisedErrorResponse(res, "TOKEN_INVALID", "Token is not valid.");
		            else{
		                req.decoded = {email: decoded.email};
		                next();
		            }
		        });
		    
		    }else{
		    	if('verifyToken' in req.query && req.query.verifyToken === 'NO') next();
		    	else TAXMANN.Utils.customisedErrorResponse(res, "AUTH_TOKEN_NOT_RECEIVED", "Auth token is not supplied.");	
		    } 
		}catch(err){
			TAXMANN.Utils.errorResponse(res, err);
		}
	}

}

module.exports = Auth;