const constants = {

	TimeZone: "Asia/Kolkata",

	RequestMethod: ['GET', 'POST', 'PUT', 'DELETE']
};


module.exports = constants;