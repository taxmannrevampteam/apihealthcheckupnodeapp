const SqlDB = require('../loaders/sqlDb');
const Jwt = require('../loaders/Jwt');
const AWSSecretsManager = require('../loaders/awsSecretsManager');

class BaseApp {
	constructor(appObj) {
		this.app = appObj;
		//set NODE Environment variable.
		//for local it will 'local', staging 'staging' and for prod undefined
		this.NODE_ENV = process.env.NODE_ENV;
		this.Configs = {};

		// this.loaders = {};
		this.Utils = {};
		this.Constants = {};
		// this._loaderClasses = {};
		this.Middlewares = {};
	}

	async initialize() {
		this.loadAppUtils();
		this.loadAppConfigs();
		this.loadAppConstants();

		this.awsSecretsManagerObj = new AWSSecretsManager();
		this.awsSecretsManagerObj.initialize();
		
		this.loadAppMiddlewares();
		await this.loadLoaderClasses();
	}

	/**
	 * @description load utilities like lodash, moment etc.
	 */
	loadAppUtils() {
		// const loader = require('../utils/index');
		// this.utils = loader(this);
		const utilityPlugins = require('../utils/plugins');
		Object.assign(this.Utils, utilityPlugins);
		const utilityFunctions = require('../utils/functions');
		Object.assign(this.Utils, utilityFunctions);
	}


	/**
	 * @description load app constants
	 */
	loadAppConstants() {
		const constants = require('../constants/constants');
		Object.assign(this.Constants, constants);
	}


	/**
	 * @description load all configs object's and put on configs property.
	 */
	loadAppConfigs() {
		const loader = require('../configs/index');
		this.Configs = loader(this);
	}

	/**
	 * @description load all configs object's and put on configs property.
	 */
	loadAppMiddlewares() {
		const Auth = require('../middlewares/auth');		
		this.Middlewares.Auth = new Auth(this);
		this.Middlewares.Auth.initialize();
	}

	/**
	 * @description load all loaders classes and created their object and put on loaders property.
	 */
	async loadLoaderClasses() {
		this.SqlDb = new SqlDB(this);
		await this.SqlDb.initialize();

		this.Jwt = new Jwt(this);
		await this.Jwt.initialize();
	}
}

module.exports = BaseApp;