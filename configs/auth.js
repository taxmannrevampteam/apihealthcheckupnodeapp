function configLoader(appObj) {
	let config = {
		JwtSecretName: getSecretName()
	};
	return config;
}


function getSecretName() {
	return 'Research/JWTDevelopmentConfig';
}


module.exports = configLoader;