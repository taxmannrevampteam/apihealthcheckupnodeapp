var fs = require('fs');
var path = require('path');

/* 
	create's an object of all Loaders Classes Configs
*/
function configLoader(appObj) {
	const appConfigs = Object.assign({}, ...fs.readdirSync(__dirname)
		.filter(file =>
			(file.indexOf(".") !== 0) && (file !== "index.js")
		)
		.map(function (file) {
			const subConfigLoader = require(path.join(__dirname, file));
			const globalObj = subConfigLoader(appObj);
			return globalObj;
		})
	);
	return appConfigs;
}

module.exports = configLoader;



