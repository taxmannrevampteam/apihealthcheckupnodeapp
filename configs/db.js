function configLoader(appObj) {
	let config = {
		SqlDbSecretName: getSecretName()
	};
	return config;
}


function getSecretName() {
	return 'Research/SQLLocalCloudConfig';
}


module.exports = configLoader;
