const plugins = {
	"moment": require('moment-timezone'),
	"lodash": require('lodash'),
	"uuid": require('uuid'),
	"jwt": require('jsonwebtoken'),
	"fetch": require('node-fetch')
};


module.exports = plugins;
