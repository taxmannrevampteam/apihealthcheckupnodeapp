const utilityFunctions = {
	successResponse(res, responseData, message='') {
		res.status(200).send({
			success: true,
			StatusMsg: message,
			ResponseType: "SUCCESS",
			Data: responseData
		});
	},
	errorResponse(res, message, statusCode=400) {
		res.status(statusCode).send({
			success: false,
			ResponseType: "ERROR",
			StatusMsg: message
		});
	},
	customisedErrorResponse(res, type, message='') {
		res.status(200).send({
			success: false,
			ResponseType: type,
			StatusMsg: message
		});
	},
	currentTimeStamp() {
		return TAXMANN.Utils.moment.tz(Date.now(), TAXMANN.Constants.TimeZone).format().slice(0,19);
	},
	reducer(acc, curVal) {
		return acc + curVal;
	}
};


module.exports = utilityFunctions;