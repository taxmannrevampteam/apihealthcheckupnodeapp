const express = require('express');
const router = express.Router();

const ApiHealthCheckUp = require('../controllers/ApiHealthCheckUpController');

/* -----------------------------------------------------------------------------------------------------------------------*/

router.get('/apiStatus', async (req, res, next) => {
	const apiHealthCheckUpObj = new ApiHealthCheckUp(req, res, next);
	await apiHealthCheckUpObj.getAllApiStatus()
		.then(result => {
			if(!result.flag) TAXMANN.Utils.customisedErrorResponse(res, result.type);	
			else TAXMANN.Utils.successResponse(res, result);
		})
		.catch(error => {
			TAXMANN.Utils.errorResponse(res, error);
		});
});


router.put('/api', async (req, res, next) => {
	const apiHealthCheckUpObj = new ApiHealthCheckUp(req, res, next);
	await apiHealthCheckUpObj.AddApi()
		.then(result => {
			if(!result.flag) TAXMANN.Utils.customisedErrorResponse(res, result.type);	
			else TAXMANN.Utils.successResponse(res, null, 'Api added successfully.');
		})
		.catch(error => {
			TAXMANN.Utils.errorResponse(res, error);
		});
});


router.post('/triggerFailedApis', async (req, res, next) => {
	const apiHealthCheckUpObj = new ApiHealthCheckUp(req, res, next);
	await apiHealthCheckUpObj.TriggerFailedApis()
		.then(result => {
			if(!result.flag) TAXMANN.Utils.customisedErrorResponse(res, result.type);	
			else TAXMANN.Utils.successResponse(res, null, 'Failed Api triggered successfully.');
		})
		.catch(error => {
			TAXMANN.Utils.errorResponse(res, error);
		});
});


router.post('/triggerApis', async (req, res, next) => {
	const apiHealthCheckUpObj = new ApiHealthCheckUp(req, res, next);
	await apiHealthCheckUpObj.TriggerApis()
		.then(result => {
			if(!result.flag) TAXMANN.Utils.customisedErrorResponse(res, result.type);	
			else TAXMANN.Utils.successResponse(res, null, 'Api triggered successfully.');
		})
		.catch(error => {
			TAXMANN.Utils.errorResponse(res, error);
		});
});


module.exports = router;