const ApiHealthCheckUpService = require('../services/ApiHealthCheckUpService');



class ApiHealthCheckUp {

	constructor(request, response, next) {
		this._request = request;
		this._response = response;
		this._next = next;
		this._apiHealthCheckUpServiceObj = new ApiHealthCheckUpService();
	}

	async getAllApiStatus() {

		let status = (this._request.query.status)?this._request.query.status:'';

		let module = (this._request.query.module)?this._request.query.module:'';

		let env = (this._request.query.env)?this._request.query.env:'';

		let apiStatuses = await this._apiHealthCheckUpServiceObj.getAllApiStatus(status, module, env).catch(err => {
			throw err;
		});

		return { flag: true, data: apiStatuses };
	}


	async AddApi() {
		if(!this._request.body.name) throw ("Api Name not received.");
		else if(!this._request.body.url) throw ("Api Url not received.");
		else if(!this._request.body.method) throw ("Api Method not received.");
		else if(!this._request.body.module) throw ("Api Module not received.");
		else if(!('token' in this._request.body)) throw ("Api Token not received.");
		else if(!('body' in this._request.body)) throw ("Api body not received.");
		else if(!TAXMANN.Constants.RequestMethod.includes(this._request.body.method)) throw ("Invalid Api method received.");
		else{

			let ApiNameExist = await this._apiHealthCheckUpServiceObj.getApiByName(this._request.body.name).catch(err => {
				throw err;
			});

			if(ApiNameExist.length > 0) return { flag: false, type: "API_NAME_EXIST" };

			let ApiUrlExist = await this._apiHealthCheckUpServiceObj.getApiByUrl(this._request.body.url).catch(err => {
				throw err;
			});

			if(ApiUrlExist.length > 0) return { flag: false, type: "API_URL_EXIST" };

			let AddApi = await this._apiHealthCheckUpServiceObj.AddApi(this._request.body).catch(err => {
				throw err;
			});

			return { flag: true };
		}
	}


	async asyncCallWithTimeout(asyncPromise, timeLimit) {
	    let timeoutHandle;

	    const timeoutPromise = new Promise((_resolve, reject) => {
	        timeoutHandle = setTimeout(
	            () => reject(new Error('Async call timeout limit reached')),
	            timeLimit
	        );
	    });

	    return Promise.race([asyncPromise, timeoutPromise]).then(result => {
	        clearTimeout(timeoutHandle);
	        return result;
	    })
	}


	async TriggerFailedApis() {

		let FailedApis = await this._apiHealthCheckUpServiceObj.GetFailedApis().catch(err => {
			throw err;
		});


		let token = 'Bearer' + TAXMANN.Utils.jwt.sign({ 
			email: 3,  
	        timeStamp: parseInt(((new Date()).getTime())),
	        nbf: parseInt(((new Date()).getTime())/1000),  
	        exp: parseInt(((new Date()).getTime())/1000) + 30*24*3600,  
	        iat: parseInt(((new Date()).getTime())/1000),  
	        iss: '873b1add-7034-4627-8783-053d47bb8148', 
	        aud: 'http://localhost:64064/'
		}, TAXMANN.Jwt.Secret);

		for(let i in FailedApis) {

			let start = TAXMANN.Utils.moment.tz(new Date(), TAXMANN.TimeZone).valueOf();
			
			let dict = {
			    method: FailedApis[i].Method, // *GET, POST, PUT, DELETE, etc.
			    mode: 'no-cors', // no-cors, *cors, same-origin
			    cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
			    credentials: 'same-origin', // include, *same-origin, omit
			    headers: {
			      'Content-Type': 'application/json'
			    },
			    redirect: 'follow', // manual, *follow, error
			    referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
			}

			if(FailedApis[i].Token) dict["headers"]["taxmannauthorization"] = token;

			if(FailedApis[i].Body) dict["body"] = FailedApis[i].Body;

			const timeoutFunc = TAXMANN.Utils.fetch(FailedApis[i].Url, dict);

			try {
		        
		        let response  = await this.asyncCallWithTimeout(timeoutFunc, 15000);

		        let end = TAXMANN.Utils.moment.tz(new Date(), TAXMANN.TimeZone).valueOf();

				if(response){

			        let responseData = await response.text();

			        responseData = (responseData.includes('"ResponseType":"SUCCESS","Status":"DATA_FOUND"'))?null:responseData;

					let statusData = {
						ApiId: FailedApis[i].ApiId,
						Status: response.status,
						Response: JSON.stringify(responseData),
						ResponseTime: end - start	
					}

					let addApiStatus = await this._apiHealthCheckUpServiceObj.AddApiStatus(statusData).catch(err => {
						throw err;
					});
				}
				
		    }catch(err) {
		        
		        let end = TAXMANN.Utils.moment.tz(new Date(), TAXMANN.TimeZone).valueOf();
				
				let statusData = {
					ApiId: FailedApis[i].ApiId,
					Status: 0,
					Response: JSON.stringify(err),
					ResponseTime: end - start	
				}

				let addApiStatus = await this._apiHealthCheckUpServiceObj.AddApiStatus(statusData).catch(err => {
					throw err;
				});
		    }	
		}

		return { flag: true };
	}


	async TriggerApis() {

		if(!('AllApiFlag' in this._request.body) || typeof(this._request.body.AllApiFlag) !== 'boolean') throw ("Api flag not received.");

		if(!this._request.body.AllApiFlag){

			if(!('ApiIdList' in this._request.body) || !Array.isArray(this._request.body.ApiIdList)) throw ("Api Id list not received.");

			if(this._request.body.ApiIdList.length === 0) throw ("Empty Api Id list received.");	
		}

		let Apis = await this._apiHealthCheckUpServiceObj.GetApisDetails(this._request.body.ApiIdList, this._request.body.AllApiFlag).catch(err => {
			throw err;
		});

		if(!this._request.body.AllApiFlag && Apis.length !== this._request.body.ApiIdList.length) throw("Api not found.");


		let token = 'Bearer' + TAXMANN.Utils.jwt.sign({ 
			email: 3,  
	        timeStamp: parseInt(((new Date()).getTime())),
	        nbf: parseInt(((new Date()).getTime())/1000),  
	        exp: parseInt(((new Date()).getTime())/1000) + 30*24*3600,  
	        iat: parseInt(((new Date()).getTime())/1000),  
	        iss: '873b1add-7034-4627-8783-053d47bb8148', 
	        aud: 'http://localhost:64064/'
		}, TAXMANN.Jwt.Secret);

		for(let i in Apis) {

			let start = TAXMANN.Utils.moment.tz(new Date(), TAXMANN.TimeZone).valueOf();
			
			let dict = {
			    method: Apis[i].Method, // *GET, POST, PUT, DELETE, etc.
			    mode: 'no-cors', // no-cors, *cors, same-origin
			    cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
			    credentials: 'same-origin', // include, *same-origin, omit
			    headers: {
			      'Content-Type': 'application/json'
			    },
			    redirect: 'follow', // manual, *follow, error
			    referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
			}

			if(Apis[i].Token) dict["headers"]["taxmannauthorization"] = token;

			if(Apis[i].Body) dict["body"] = Apis[i].Body;

			const timeoutFunc = TAXMANN.Utils.fetch(Apis[i].Url, dict);

			try {
		        
		        let response  = await this.asyncCallWithTimeout(timeoutFunc, 15000);

		        let end = TAXMANN.Utils.moment.tz(new Date(), TAXMANN.TimeZone).valueOf();

				if(response){

			        let responseData = await response.text();

			        responseData = (responseData.includes('"ResponseType":"SUCCESS","Status":"DATA_FOUND"'))?null:responseData;

					let statusData = {
						ApiId: Apis[i].ApiId,
						Status: response.status,
						Response: JSON.stringify(responseData),
						ResponseTime: end - start	
					}

					let addApiStatus = await this._apiHealthCheckUpServiceObj.AddApiStatus(statusData).catch(err => {
						throw err;
					});
				}
				
		    }catch(err) {
		        
		        let end = TAXMANN.Utils.moment.tz(new Date(), TAXMANN.TimeZone).valueOf();
				
				let statusData = {
					ApiId: Apis[i].ApiId,
					Status: 0,
					Response: JSON.stringify(err),
					ResponseTime: end - start	
				}

				let addApiStatus = await this._apiHealthCheckUpServiceObj.AddApiStatus(statusData).catch(err => {
					throw err;
				});
		    }	
		}

		return { flag: true };
	}


}

module.exports = ApiHealthCheckUp;