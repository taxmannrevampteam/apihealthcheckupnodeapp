class ApiHealthCheckUpService {


	async getAllApiStatus(status, module, env) {

		let query = `[usp_GetApiStatusLogs] @Status = '${status}', @Module = '${module}', @Env = '${env}'`;

		return await TAXMANN.SqlDb.QueryObj.query(query)
			.then(rec => {
				return rec.recordset;
			}).catch(err => {
				throw err;
			});	
	}

	async getApiByName(Name) {

		let query = `SELECT 
						ApiId 
					FROM 
						Api 
					WHERE Name = '${Name}'`;

		return await TAXMANN.SqlDb.QueryObj.query(query)
			.then(rec => {
				return rec.recordset;
			}).catch(err => {
				throw err;
			});	
	}


	async getApiByUrl(Url) {

		let query = `SELECT 
						ApiId 
					FROM 
						Api 
					WHERE Url = '${Url}'`;

		return await TAXMANN.SqlDb.QueryObj.query(query)
			.then(rec => {
				return rec.recordset;
			}).catch(err => {
				throw err;
			});	
	}


	async AddApi(data) {

		let query = `INSERT INTO Api (Name, Url, Method, Module, Body, Token, CreatedAt) 
						VALUES(
							'${data.name}',
							'${data.url}',
							'${data.method}',
							'${data.module}',`;
		query += (data.body)?`'${JSON.stringify(data.body)}',`:`null,`;
		
		query +=	`${data.token},
						'${TAXMANN.Utils.currentTimeStamp()}')`;

		return await TAXMANN.SqlDb.QueryObj.query(query)
			.then(rec => {
				return rec.recordset;
			}).catch(err => {
				throw err;
			});	
	}


	async GetFailedApis() {

		let query = `[usp_GetApiStatusLogs] @Status = 'FAILED', @Module = '', @Env = ''`;
		
		return await TAXMANN.SqlDb.QueryObj.query(query)
			.then(rec => {
				return rec.recordset;
			}).catch(err => {
				throw err;
			});		
	}


	async GetApisDetails(ApiIdList, flag) {

		let ApiIdStr = '';

		let count = 0;

		if(!flag){

			ApiIdList.map((ApiId) => {
				count += 1;
				ApiIdStr += `${ApiId}` + ((count === ApiIdList.length)?``:`, `);
			});
		}
			

		let query = `SELECT 
					    ApiId, Url, Method, Token, Body 
					FROM 
					    Api
					WHERE 1 = 1 `;

		if(!flag) query += ` AND ApiId IN (${ApiIdStr})`;
		
		return await TAXMANN.SqlDb.QueryObj.query(query)
			.then(rec => {
				return rec.recordset;
			}).catch(err => {
				throw err;
			});		
	}


	async AddApiStatus(data) {
		
		let query = `INSERT INTO 
						
					ApiStatus(ApiId, Status, Response, ResponseTime, CreatedAt)
					
					VALUES(
						${data.ApiId},
						${data.Status},
						'${data.Response}',
						${data.ResponseTime},
						'${TAXMANN.Utils.currentTimeStamp()}'
					)`;

		return await TAXMANN.SqlDb.QueryObj.query(query)
			.then(rec => {
				return rec.recordset;
			}).catch(err => {
				throw err;
			});		
	}

}

module.exports = ApiHealthCheckUpService;