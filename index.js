const express = require('express');
// const logger = require('morgan');
const cors = require('cors')

const appObj = express();

// appObj.use(logger('dev'));
appObj.use(cors())

appObj.use(express.json());
appObj.use(express.urlencoded({ extended: true }));

const BaseAppClass = require('./main/baseApp');
global.TAXMANN = new BaseAppClass(appObj);
TAXMANN.initialize();

const ApiHealthCheckUpRoutes = require('./routes/ApiHealthCheckUp.route');
appObj.use('/apiHealthCheckUp', ApiHealthCheckUpRoutes);

module.exports = TAXMANN.app



